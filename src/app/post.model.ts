


export class Post {
  title: string;
  post: string;
  votes: number;

  constructor(title: string, post: string, votes?: number) {
    this.title = title;
    this.post = post;
    this.votes = votes || 0;
  }

  voteUp(): void {
    this.votes += 1;
  }

  voteDown(): void {
    this.votes -= 1;
  }

}