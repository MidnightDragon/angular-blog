import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../post.model';
import { AppService } from '../app.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
    @Input() childPost: Post;
    @Output() public childEvent = new EventEmitter();

    constructor(private appService: AppService) { }

    ngOnInit() {
    }

    voteUp(): boolean {
        this.childPost.voteUp();
        return false;
    }

    voteDown(): boolean {
        this.childPost.voteDown();
        return false;
    }

    // delete post using Output
    deletePost() {
        this.childEvent.emit(this.childPost);
    }

    // delete post using services
    deletePostServices() {
        this.appService.postDeleted.emit(this.childPost);
    }

}
