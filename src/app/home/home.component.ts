import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [AppService]
})
export class HomeComponent implements OnInit {

    posts: Post[];

    constructor(
        private router: Router,
        private appService: AppService 
    ) { }

    ngOnInit(): void {

        this.appService.postDeleted.subscribe(
            (childPost: Post) => {
                let index = this.posts.indexOf(childPost);
                this.posts.splice(index, 1);
                console.log(index + ' yolo');
            }
        );

        this.posts = [
            new Post('title 1', 'post 1', 5),
            new Post('title 2', 'post 2', 3),
        ];
    }

    addPost(title: HTMLInputElement, post: HTMLInputElement): boolean {
        this.posts.push(new Post(title.value, post.value, 0));
        console.log(this.posts);
        title.value = '';
        post.value = '';

        return false;
    }

    deletePost(childPost) {
        let index = this.posts.indexOf(childPost);
        this.posts.splice(index, 1);
        console.log(index);

    }

}
